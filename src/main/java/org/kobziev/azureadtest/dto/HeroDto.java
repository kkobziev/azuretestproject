package org.kobziev.azureadtest.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class HeroDto {
    private long id;
    private String name;
}
