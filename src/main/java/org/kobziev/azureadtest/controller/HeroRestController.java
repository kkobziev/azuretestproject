package org.kobziev.azureadtest.controller;

import org.kobziev.azureadtest.dto.HeroDto;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Set;

import static java.util.Arrays.asList;

@RestController
public class HeroRestController {

    private static final List<HeroDto> HEROES = asList(
            new HeroDto(0, "Tornado"),
            new HeroDto(1, "Dr Nice"),
            new HeroDto(2, "Narco"),
            new HeroDto(3, "Bombasto"),
            new HeroDto(4, "Celeritas"),
            new HeroDto(5, "Magneta"),
            new HeroDto(6, "RubberMan"),
            new HeroDto(7, "Dynama"),
            new HeroDto(8, "Dr IQ"),
            new HeroDto(9, "Magma")
    );

    @PreAuthorize("hasRole('ROLE_group1')")
    @GetMapping(value = "/api/heroes", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<HeroDto> getHeroes() {
        return HEROES;
    }

    @GetMapping("/api/roles")
    public Set<String> getRoles(Authentication authentication) {
        return AuthorityUtils.authorityListToSet(authentication.getAuthorities());
    }

    @GetMapping(value = "/api/heroes/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public HeroDto getHeroes(@PathVariable("id") int id) {
        return HEROES.get(id);
    }
}

